import { Link } from "react-router-dom";
import {useLocation} from "react-router-dom";
import {useState,useEffect } from "react";
import axios from "axios";
import "./singlePost.css";

export default function SinglePost() {
  const [post,setPost] = useState({});
  const location = useLocation();
  const path = location.pathname.split("/")[2];
  const url = "http://localhost:8000/api/movies/"+path
  useEffect(()=>{
    axios({
      url: url,
      method: "GET",
    }).then((res) => { 
      // console.log(res.data[0].title);
      setPost(res.data[0]);
    })
    .catch((err) => { 
      console.log(err);
    });
  },[path])

  return (
    <div className="singlePost">
      <div className="singlePostWrapper">
        <img
          className="singlePostImg"
          src="https://api.lorem.space/image/movie?w=150&amp;amp;amp;amp;h=220"
          alt=""
        />
        <h1 className="singlePostTitle">
          {post.title}
          {/* <div className="singlePostEdit">
            <i className="singlePostIcon far fa-edit"></i>
            <i className="singlePostIcon far fa-trash-alt"></i>
          </div> */}
        </h1>
        <div className="singlePostInfo">
          <span>
            Category:
            <b className="singlePostAuthor">
              <Link className="link" >
                {post.cat_name}
              </Link>
            </b>
          </span>
          <span>{new Date(post.created_at).toDateString()}</span>
        </div>
        <p className="singlePostDesc">
          {post.description}
        </p>
      </div>
    </div>
  );
}
