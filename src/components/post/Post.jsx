import { Link } from "react-router-dom";
import "./post.css";

export default function Post({post}) {
  return (
    <div className="post">
      <img
        className="postImg"
        src="https://api.lorem.space/image/movie?w=150&amp;amp;amp;amp;h=220"
        alt=""
      />
      <div className="postInfo">
        <div className="postCats">
          <span className="postCat">
            <Link className="link" >
              {post.cat_name}
            </Link>
          </span>
        </div>
        <span className="postTitle">
          <Link to={`/post/${post.id}`} className="link">
            {post.title}
          </Link>
        </span>
        <hr />
        <span className="postDate">{new Date(post.created_at).toDateString()}</span>
      </div>
      <p className="postDesc">
        {post.description}
      </p>
    </div>
  );
}
