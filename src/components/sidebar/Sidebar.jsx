import { Link } from "react-router-dom";
import {useState,useEffect } from "react";
import axios from "axios";
import "./sidebar.css";

export default function Sidebar() {
  const [categories,setCategories] = useState([]);
  useEffect(()=>{
    axios({
      url: "http://localhost:8000/api/categories/",
      method: "GET",
    }).then((res) => { 
      // console.log(res);
      setCategories(res.data);
    })
    .catch((err) => { 
      console.log(err);
    });
  },[])
  return (
    <div className="sidebar">
      <div className="sidebarItem">
        <span className="sidebarTitle">CATEGORIES</span>
        <ul className="sidebarList">
          {categories.map((c)=>(
            <li className="sidebarListItem">
              <Link className="link">
                {c.name}
              </Link>
            </li>
          ))}
          
          
        </ul>
      </div>
      <div className="sidebarItem">
        <span className="sidebarTitle">FOLLOW US</span>
        <div className="sidebarSocial">
          <i className="sidebarIcon fab fa-facebook-square"></i>
          <i className="sidebarIcon fab fa-instagram-square"></i>
          <i className="sidebarIcon fab fa-pinterest-square"></i>
          <i className="sidebarIcon fab fa-twitter-square"></i>
        </div>
      </div>
    </div>
  );
}
