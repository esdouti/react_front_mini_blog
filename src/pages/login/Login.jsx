import { Link } from "react-router-dom";
import { useState,useContext } from "react";
import { Context } from "../../context/Context";
import axios from "axios";
import "./login.css";

export default function Login() {

  const [registerPassword, setRegisterPassword] = useState("");
  const [registerEmail, setRegisterEmail] = useState("");
  const [msg, setMsg] = useState();
  const {dispatch, isFetching} = useContext(Context);


  const login = async (e) => {
    e.preventDefault();
    dispatch({type:"LOGIN_START"});
    var formData = {
      "email" : registerEmail, 
      "password" : registerPassword
    };

    axios({
      url: "http://localhost:8000/api/login",
      method: "POST",
      // headers: {authorization: "your token comes here"},
      data: formData,
    }).then((res) => { 
      // console.log(res);
      setMsg(res.data.message);
      dispatch({type:"LOGIN_SUCCESS",payload:res.data.user});
      // setTimeout(function(){window.location="/";}, 2000)
    })
    .catch((err) => { 
      console.log(err);
      dispatch({type:"LOGIN_FAILURE"});
    });
  
  };
  return (
    <div className="login">
          {msg && <span class="msg">{msg}</span>}   

      <span className="loginTitle">Login</span>
      <form className="loginForm">
        <label>Email</label>
        <input className="loginInput" type="text" placeholder="Enter your email..." onChange={(event) => {setRegisterEmail(event.target.value);}}/>
        <label>Password</label>
        <input className="loginInput" type="password" placeholder="Enter your password..." onChange={(event) => {setRegisterPassword(event.target.value);}}/>
        <button className="loginButton" onClick={login} disabled={isFetching}>Login</button>
      </form>
        <button className="loginRegisterButton">
          <Link className="link" to="/register">
            Register
          </Link>
          
        </button>
    </div>
  );
}
