import { Link } from "react-router-dom";
import { useState,useContext } from "react";
import { Context } from "../../context/Context";
import axios from "axios";
import "./register.css"

export default function Register() {
  const {dispatch, isFetching} = useContext(Context);
  const [registerFirstname, setRegisterFirstname] = useState("-");
  const [registerLastname, setRegisterLastname] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [registerEmail, setRegisterEmail] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [msg, setMsg] = useState();


  const register = async (e) => {
    e.preventDefault();
    dispatch({type:"LOGIN_START"});
    var formData = {
      "firstname" : registerFirstname,
      "lastname" : registerLastname, 
      "email" : registerEmail, 
      "password" : registerPassword, 
      "confirmation" : passwordConfirm
    };

    axios({
      url: "http://localhost:8000/api/users",
      method: "POST",
      // headers: {authorization: "your token comes here"},
      data: formData,
    }).then((res) => { 
      // console.log(res);  
      setMsg(res.data.message);
      dispatch({type:"LOGIN_SUCCESS",payload:res.data.user});
      // setTimeout(function(){window.location="/";}, 2000)

    })
    .catch((err) => { 
      console.log(err);
    });
  
  };



    return (
        <div className="register">
          {msg && <span class="msg">{msg}</span>}         
      <span className="registerTitle">Register</span>
      <form className="registerForm">
        <label>Username</label>
        <input className="registerInput" type="text" placeholder="Enter your username..." onChange={(event) => {setRegisterLastname(event.target.value);}} />

        <label>Email</label>
        <input className="registerInput" type="text" placeholder="Enter your email..." onChange={(event) => {setRegisterEmail(event.target.value);}} />

        <label>Password</label>
        <input className="registerInput" type="password" placeholder="Enter your password..." onChange={(event) => {setRegisterPassword(event.target.value);}} />

        <label>Confirm Password</label>
        <input className="registerInput" type="password" placeholder="Confirm your password..." onChange={(event) => {setPasswordConfirm(event.target.value);}} />

        <button className="registerButton" onClick={register}>Register</button>
      </form>
        <button className="registerLoginButton">
          <Link className="link" to="/login">
            Login
          </Link>
        </button>
    </div>
    )
}
