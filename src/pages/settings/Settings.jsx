import "./settings.css";
import Sidebar from "../../components/sidebar/Sidebar";
import {useContext,useState} from "react";
import { Context } from "../../context/Context";
import axios from "axios";

export default function Settings() {
  const {user} = useContext(Context);


  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [msg, setMsg] = useState();
  const {dispatch, isFetching} = useContext(Context);

  const handleSubmit = async (e) => {
    console.log()
    e.preventDefault();
    dispatch({ type: "UPDATE_START" });
    var formData = {
      "email" : email, 
      "lastname": username,
      "firstname":"-",
      "password" : password,
      "confirmation" : password
    };

    axios({
      url: "http://localhost:8000/api/users/"+user.id,
      method: "PUT",
      // headers: {authorization: "your token comes here"},
      data: formData,
    }).then((res) => { 
      // console.log(res);
      setMsg(res.data.message);
      dispatch({ type: "UPDATE_SUCCESS", payload: res.data.user });
      // setTimeout(function(){window.location="/";}, 2000)
    })
    .catch((err) => { 
      console.log(err);
      // setMsg(res.data.message);
      dispatch({ type: "UPDATE_FAILURE" });
    });
    //
  }

  return (
    <div className="settings">
      <div className="settingsWrapper">
        <div className="settingsTitle">
          <span className="settingsTitleUpdate">Update Your Account</span>
          {/* <span className="settingsTitleDelete">Delete Account</span> */}
        </div>
        <form className="settingsForm">
          {/* <label>Profile Picture</label>
          <div className="settingsPP">
            <img
              src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
              alt=""
            />
            <label htmlFor="fileInput">
              <i className="settingsPPIcon far fa-user-circle"></i>{" "}
            </label>
            <input
              id="fileInput"
              type="file"
              style={{ display: "none" }}
              className="settingsPPInput"
            />
          </div> */}
          {msg && <span class="msg">{msg}</span>}  
          <label>Username</label>
          <input type="text" placeholder={user.lastname}  onChange={(e) => setUsername(e.target.value)}/>
          <label>Email</label>
          <input type="email" placeholder={user.email}  onChange={(e) => setEmail(e.target.value)}/>
          <label>Password</label>
          <input type="password" placeholder="Password"  onChange={(e) => setPassword(e.target.value)}/>
          <button className="settingsSubmitButton" type="submit" onClick={handleSubmit}>
            Update
          </button>
        </form>
      </div>
      <Sidebar />
    </div>
  );
}
