import { useLocation } from "react-router";
import { useState,useEffect } from "react";
import axios from "axios";
import Header from "../../components/header/Header";
import Posts from "../../components/posts/Posts";
import Sidebar from "../../components/sidebar/Sidebar";
import "./homepage.css";

export default function Homepage() {
  const [posts,setPosts] = useState([]);

  // const location = useLocation();
  // console.log(location);


useEffect(()=>{
  axios({
    url: "http://localhost:8000/api/movies",
    method: "GET",
  }).then((res) => { 
    // console.log(res);
    setPosts(res.data);
  })
  .catch((err) => { 
    console.log(err);
  });
},[])

  return (
    <>
      <Header />
      <div className="home">
        <Posts posts={posts}/>
        <Sidebar />
      </div>
    </>
  );
}
